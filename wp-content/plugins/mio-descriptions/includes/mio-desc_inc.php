<?php
   function desc_add_scripts ()
   {
       //CSS
       wp_register_style('mio-desc-style', plugins_url( ) . '/mio-descriptions/includes/less/dist/style.css');
       wp_enqueue_style('mio-desc-style');
   
       //JS
       wp_enqueue_script('mio-desc-script', plugins_url( ) . '/mio-descriptions/includes/js/main.js');
   }
   
   add_action('admin_head', 'desc_add_scripts');