<?php
/*
    Plugin Name: Mio Descriptions
    Description: Permet la personnalisation des éléments de description des différentes parties du site.
    Version: 1.0.0
    Author: Stéphane Longeaux
*/

require_once (plugin_dir_path(__FILE__) . '/includes/mio-desc_inc.php');
require_once (plugin_dir_path(__FILE__) . '/includes/mio-desc-class_inc.php');

//Register Widget

function register_mio_description() {
    register_widget( 'Section_Desc_Widget' );
}

add_action('widgets_init', 'register_mio_description');

register_activation_hook( __FILE__, function () {
    touch(__DIR__ . '/mio_desc');
});

register_deactivation_hook( __FILE__, function () {
    unlink(__DIR__ . '/mio_desc');
});

add_action('init', function () {
    register_post_type('descriptions', [
        'label' => 'Description',
        'public' => true,
        'supports' => ['title', 'editor', 'thumbnail']
    ]);
});





