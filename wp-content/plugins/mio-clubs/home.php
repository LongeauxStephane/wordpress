<?php
    require_once (plugin_dir_path(__FILE__) . '/includes/mio-clubs_inc.php');
    require_once (plugin_dir_path(__FILE__) . '/includes/mio-clubs-class_inc.php');
?>




<section class="admin-gestion">

    <h1>Mio</h1>

    <div class="box-main-panel">

        <div class="box-clubs">
            <div class="row">
                <h3>Liste des clubs</h3>
            </div>
        
            <div class="tableau">        
                <div class="row tab-header">        
                    <div class="col-lg-1">
                        <span>Selection</span>           
                    </div>
                    <div class="col-lg-3">
                        <span>Nom</span>           
                    </div>
                    <div class="col-lg-3">
                        <span>Adresse</span>
                    </div>
                    <div class="col-lg-2">
                        <span>Code Postal</span>
                    </div>
                    <div class="col-lg-1">
                        <span>Téléphone</span>
                    </div>
                    <div class="col-lg-1">
                        <span>Domaine</span>
                    </div>
                </div>
                
        
                <div class="clubs-list">
                    <?php clubs_list(); ?>
                </div>
        
        
        
                <div class="clubs-form">
                    <?php clubs_add (); ?>
                </div> 
            </div><!-- Fin Tableau -->
           
        </div><!-- Fin Clubs -->
    

    <div class="box-competitions">
        <div class="row">
            <h3>Compétitions</h3>
        </div>

        <div class="tableau">        
            <div class="row tab-header">        
                <div class="col-lg-12 col-md-12 col-12" >
                    <span>Ajouter une compétition</span>           
                </div>          
            </div>

            
            <div class="clubs-list">
                <?php comp_add (); ?>
            </div>

            <div class="clubs-list">
                <?php add_comp_adh(); ?>
                <?php adh_selectected(); ?>
            </div>

            <div class="row tab-header">        
                <div class="col-lg-6 col-md-6 col-6" >
                    <span>Liste des participants</span>           
                </div>  
                <div class="col-lg-6 col-md-6 col-6" >     
                    <form action="" method="POST" class="formulaire-ajout">
                        <?php comp_list_choice(); ?>
                        <button type="submit" name="btn-adh-comp-list">Voir</button>
                    </form>                  
                </div>    
            </div>            
            <?php list_part(); ?>
           

        </div><!-- Fin Tableau -->



    </div>



    </div><!-- Fin box main -->
       




   










<?php

/*=================================================================================================================*/
/*============================================= PANEL MIO =======================================================*/
/*===============================================================================================================*/

/*===============================*/
/*========== GENERAL ===========*/
/*=============================*/


/*** Data Base ***/
$servername = "localhost";
$username = "root";
$password = "";

// Create connection
$conn = new mysqli($servername, $username, $password, 'wordpress');


/*=================================================================================================================*/
/*============================================= FUNCTIONS ========================================================*/
/*===============================================================================================================*/


/*=========================================*/
/*=============== CLUBS ==================*/
/*=======================================*/


/* Liste des clubs */

function clubs_list()
{

/*** Data Base ***/
$servername = "localhost";
$username = "root";
$password = "";

// Create connection
$conn = new mysqli($servername, $username, $password, 'wordpress');

 
$query_clubs = "SELECT * FROM wp_clubs";
$rslt_clubs = mysqli_query($conn, $query_clubs);

echo '<form action="" method="POST" name="form-list-clubs" id="form-list-clubs">';

    while ( $rows = mysqli_fetch_assoc($rslt_clubs) ) {
        $club_id = $rows['Id'];
        $club_name = utf8_encode($rows['nom']);
        $club_adress = utf8_encode($rows['adresse']);
        $club_cp = $rows['cp'];
        $club_mail = $rows['email'];
        $club_tel = $rows['tel'];
        $club_domaine = $rows['domaine'];
            

echo <<< CLUBS_LIST
 
    <div class="row">
        <div class="col-lg-1 col-md-1 col-1">
            <input type="checkbox" id="$club_id" name="club_pick[]" value="$club_name" >
        </div>
        <div class="col-lg-3 col-md-3 col-3">
            <span class="cell">$club_name</span>
        </div>
        <div class="col-lg-3 col-md-3 col-3">
            <span class="cell">$club_adress</span>
        </div>
        <div class="col-lg-1 col-md-1 col-1">
            <span class="cell">$club_cp</span>
        </div>
        <div class="col-lg-1 col-md-1 col-1">
            <span class="cell">$club_tel</span>
        </div>
        <div class="col-lg-2 col-md-2 col-2">
            <span class="cell">$club_domaine</span>
        </div>
    </div>  
   

               
CLUBS_LIST;  


    }/*Fin while*/

    echo '</form> ';
}
?>

<!-- ========== Ajout de clubs ========= -->


<?php
    /* Ajout de clubs*/
    if ( isset($_POST['btn_clubs_add']) ) {      
        $club_name_added = $_POST['club_name_add'];
        $club_adresse_added = $_POST['club_adresse_add'];
        $club_cp_added = $_POST['club_cp_add'];
        $club_tel_added = $_POST['club_tel_add'];
        $club_domaine_added = $_POST['club_domaine_add'];


        $add_club_name = "INSERT INTO wp_clubs(nom, adresse, cp, tel, domaine) VALUES('$club_name_added', '$club_adresse_added', '$club_cp_added', '$club_tel_added', '$club_domaine_added')";
        $add_club_rslt = mysqli_query($conn, $add_club_name);      
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);        
    }   

    /*Supression d'utilisateurs*/
    if ( isset( $_POST['btn-supprimer-club']) ) {
        var_dump($_POST['club_pick']);
        if ( !empty($_POST['club_pick']) ) {
            foreach($_POST['club_pick'] as $picked) { 
                var_dump($picked);
                $_SESSION['club_picked'] = $picked;
                $erase_club_query = "DELETE FROM wp_clubs WHERE `nom`= '$picked'";
                $erase_club_rslt = mysqli_query($conn, $erase_club_query);
            }
        }
    }

    function clubs_add ()
    {
        if ( !isset($_GET["btn-ajouter"]) ) {            
            echo <<< clubS_ADD
                <form action="" method="POST" class="formulaire-ajout">                   
                    <div class="row">                   
                        <div class="col-lg-2 col-md-2 col-2">
                            <label for="club_name_add">Nom du club</label>
                                <input type="text" name="club_name_add">  
                        </div>      
                        <div class="col-lg-2 col-md-2 col-2">
                            <label for="club_adresse_add">Adresse</label>
                                <input type="text" name="club_adresse_add"> 
                        </div>       
                        <div class="col-lg-2 col-md-2 col-2">
                            <label for="club_cp_add">Code postale</label>
                                <input type="text" name="club_cp_add">
                        </div>    
                        <div class="col-lg-2 col-md-2 col-2">
                            <label for="club_tel_add">Téléphone</label>
                                <input type="text" name="club_tel_add">      
                        </div>                 
                        <div class="col-lg-2 col-md-2 col-2">
                            <select name="club_domaine_add" id="">
                                <option value="automobile" name="automobile">Automobile</option>
                                <option value="aerien" name="aerien">Aérien</option>              
                                <option value="naval" name="naval">Naval</option>              
                            </select>    
                        </div>                                   
                    </div>
                    <div class="row"> 
                        <div class="offset-8"></div>
                        <div class="col-lg-2 col-md-2 col-2">
                            <button type="submit" name="btn_clubs_add" >Ajouter</button>  
                        </div>                         
                        <div class="col-lg-2 col-md-2 col-2">
                            <button type="submit" name="btn-supprimer-club" form="form-list-clubs">Supprimer</button>  
                        </div>    
                    </div>
                </form>
                
            clubS_ADD; 
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);       
        }

    }





/*=========================================*/
/*=============== comp ==================*/
/*=======================================*/


/* Liste des comp */

function comp_list()
{

/*** Data Base ***/
$servername = "localhost";
$username = "root";
$password = "";

// Create connection
$conn = new mysqli($servername, $username, $password, 'wordpress');

 
$query_comp = "SELECT * FROM wp_competitions";
$rslt_comp = mysqli_query($conn, $query_comp);

    while ( $rows = mysqli_fetch_assoc($rslt_comp) ) {
        $comp_id = $rows['id'];
        $comp_name = utf8_encode($rows['nom']);
        $comp_adress = utf8_encode($rows['adresse']);
        $comp_cp = $rows['cp'];
        $comp_mail = $rows['email'];
        $comp_tel = $rows['tel'];
        $comp_domaine = $rows['domaine'];
            

echo <<< comp_LIST
 
<div class="row">
    <div class="col-lg-1 col-md-1 col-1">
        <input type="checkbox" id="$comp_id" name="user_pick[]" >
    </div>
    <div class="col-lg-3 col-md-3 col-3">
        <span class="cell">$comp_name</span>
    </div>
    <div class="col-lg-3 col-md-3 col-3">
        <span class="cell">$comp_adress</span>
    </div>
    <div class="col-lg-1 col-md-1 col-1">
        <span class="cell">$comp_cp</span>
    </div>
    <div class="col-lg-1 col-md-1 col-1">
        <span class="cell">$comp_tel</span>
    </div>
    <div class="col-lg-2 col-md-2 col-2">
        <span class="cell">$comp_domaine</span>
    </div>
</div>      

               
comp_LIST;  


    }/*Fin while*/
}
?>

<!-- ========== Ajout de comp ========= -->


<?php
    /* Ajout de comp*/
    if ( isset($_POST['btn_comp_add']) ) {      
        $comp_name_added = $_POST['comp_name_add'];
        $comp_date_added = $_POST['comp_date_add'];
        $comp_domaine_added = $_POST['comp_domaine_add'];


        $add_comp = "INSERT INTO wp_competitions(nom, `date`, domaine) VALUES('$comp_name_added', '$comp_date_added', '$comp_domaine_added')";
        $add_comp_rslt = mysqli_query($conn, $add_comp);      
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);        
    }   

    function comp_add ()
    {
        if ( !isset($_GET["btn-ajouter"]) ) {            
            echo <<< comp_ADD
                <div class="row">
                    <form action="" method="POST" class="formulaire-ajout">
                        <div class="col-lg-12 col-md-12 col-12">
                        <select name="comp_domaine_add" id="">
                            <option value="automobile" name="automobile">Automobile</option>
                            <option value="aerien" name="aerien">Aérien</option>              
                            <option value="naval" name="naval">Naval</option>              
                        </select>    
                            <label for="comp_name_add">Nom</label>
                                <input type="text" name="comp_name_add">        
                            <label for="comp_date_add">Date</label>
                                <input type="date" name="comp_date_add">                                         
                            <button type="submit" name="btn_comp_add" >Ajouter</button>      
                        </div>            
                    </form>
                </div>
                
            comp_ADD; 
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);       
            
        }

    }

    function comp_list_choice() {

        /*** Data Base ***/
        $servername = "localhost";
        $username = "root";
        $password = "";

        // Create connection
        $conn = new mysqli($servername, $username, $password, 'wordpress');
        
        $query_comp = "SELECT * FROM wp_competitions";
                $rslt_comp = mysqli_query($conn, $query_comp);

        
        while ( $rows = mysqli_fetch_assoc($rslt_comp) ) {
            $comp_name = $rows['nom'];
            $comp_id = $rows['id'];
            $comp_domaine = $rows['domaine'];
            ;
            $options_comp[]= "<option value=" . $comp_name ." name=" . $comp_name ." domaine=" . $comp_domaine . " class=" . 'opt-tag-dom' . ">$comp_name</option>";           
        } 

        $options_comp = implode(' ' , $options_comp);     
        
        echo <<<LIST_COMP
            <label>Choisir la compétition : </label>                 
            <select name="comp_name" id="comp-select-choice">                                           
                $options_comp;
            </select> 
LIST_COMP;

    }

    function add_comp_adh()
    {
        /*** Data Base ***/
        $servername = "localhost";
        $username = "root";
        $password = "";

        // Create connection
        $conn = new mysqli($servername, $username, $password, 'wordpress');
        
        $query_comp = "SELECT * FROM wp_competitions";
        $rslt_comp = mysqli_query($conn, $query_comp);

        $query_comp_tag = "SELECT domaine FROM wp_competitions WHERE nom = '$comp_name' ";
        $rslt_comp_tag = mysqli_query($conn, $query_comp_tag);

        $query_clubs_name = "SELECT nom FROM wp_clubs";
        $rslt_clubs_name = mysqli_query($conn, $query_clubs_name);

        $query_adh_picked = "SELECT * FROM wp_membres";
        $rslt_adh_picked = mysqli_query($conn, $query_adh_picked);

   
        
        // while ( $rows = mysqli_fetch_assoc($rslt_comp_tag) ) {
        //     $comp_domaine = $rows['domaine'];
        // }

        while ( $rows = mysqli_fetch_assoc($rslt_comp) ) {
            $comp_name = $rows['nom'];
            $comp_id = $rows['id'];
            $comp_domaine = $rows['domaine'];
            ;
            $options_comp[]= "<option value=" . $comp_name ." name=" . $comp_name ." domaine=" . $comp_domaine . " class=" . 'opt-tag-dom' . ">$comp_name</option>";           
        } 

        $options_comp = implode(' ' , $options_comp);        
        

        while ( $rows = mysqli_fetch_assoc( $rslt_clubs_name) ) {
            $club_name = $rows['nom'];
            $options_clubs[]= "<option value='" . $club_name ."' name='" . 'choix' ."[]'>$club_name</option>";           
        } 

        $options_clubs = implode(' ' , $options_clubs);    
        

        echo <<<ADD_COMP_ADH

         

            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <form action="" method="POST" class="formulaire-ajout" id="form-adh-add-comp">  
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-12">
                                <label>Choisir la compétition : </label>                 
                                <select name="comp_name" id="comp-select">                                           
                                    $options_comp;
                                </select> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-3">
                                <label for="part_name_add">Nom</label>
                                    <input type="text" name="part_name">   
                            </div>   
                            <div class="col-lg-3 col-md-3 col-3">  
                                <label for="part_prenom">Prenom</label>
                                    <input type="text" name="part_prenom">  
                            </div>
                            <div class="col-lg-3 col-md-3 col-3">
                                <label for="part_club">Choisir le club : </label>                            
                                    <select name="part_club" id="comp-select">   
                                        $options_clubs;
                                    </select> 
                            </div> 
                            <div class="col-lg-3 col-md-3 col-3">
                                <button type="submit" name="btn-part-adh-comp"> Ajouter </button>
                            </div> 
                        </div>
                    </form>
                </div> 
            </div> 

          
ADD_COMP_ADH;   
        
    }/* END FINAL FUNCTION */




/************************************************************************ */


function adh_selectected ()
{
    /*** Data Base ***/
    $servername = "localhost";
    $username = "root";
    $password = "";

    // Create connection
    $conn = new mysqli($servername, $username, $password, 'wordpress');

    if ( $_POST['choix']) {
        $query_adh_name = "SELECT * FROM wp_membres WHERE `club` IN ( '" . implode("','" , $_POST['choix'] ) . "'); ";
        $rslt_adh_name = mysqli_query($conn, $query_adh_name);
    };

    if ( isset($_POST['clubs_selected'])) {     
        echo ' <form action="" method="POST" class="formulaire-ajout" id="participants"> ';
            while ( $rows = mysqli_fetch_assoc( $rslt_adh_name) ) {
                $adh_id = $rows['id'];
                $adh_prenom = $rows['prenom'];
                $adh_name = $rows['nom'];
                $adh_club = $rows['club'];

                echo <<<TT
                    
                        
                        <div class="row">
                            <div class="col-lg-1 col-md-1 col-1">
                                <input type="checkbox" id="$adh_id" value="$adh_id" name="part_pick[]" >
                            </div>
                            <div class="col-lg-3 col-md-3 col-3">
                                <span class="cell"> $adh_club</span>
                            </div>
                            <div class="col-lg-3 col-md-3 col-3">
                                <span class="cell">$adh_prenom</span>
                            </div>
                            <div class="col-lg-3 col-md-3 col-3">
                                <span class="cell">$adh_name</span>
                            </div>
                            </div>
                                        
                    
    TT;                
            } 
        echo '</form>';
        echo '<div class="form-btn btn-participants"> <button type="submit" form="participants" name="participants_selected">Valider</button></div>';
    }
}

/************************************************************************ */

if ( isset($_POST['participants_selected']) ) {
    echo '<h2>Liste des participants:</h2>';

        echo <<<TET
            <div class="tableau">        
                <div class="row tab-header">        
                    <div class="col-lg-3">
                        <span>Club</span>           
                    </div>
                    <div class="col-lg-3">
                        <span>Prenom</span>           
                    </div>
                    <div class="col-lg-3">
                        <span>Nom</span>
                    </div>
                    <div class="col-lg-1">
                        <span>Score</span>
                    </div>    
                </div>
            </div>
        TET;

    if ( !empty($_POST['part_pick']) ) {
        foreach($_POST['part_pick'] as $picked){    
            $_SESSION['part_picked'] = $picked;    
        }            
    }

    echo ' <form action="" method="POST" class="formulaire-ajout" id="participants_scores"> ';

    while ( $rows = mysqli_fetch_assoc( $rslt_adh_picked) ) {
        $adh_id = $rows['id'];
        $adh_prenom = $rows['prenom'];
        $adh_name = $rows['nom'];          

        if ( isset ($_SESSION['part_picked']) ){
            if(in_array($adh_id,  $_POST['part_pick'])) { 
                foreach ($rows as $key=>$value) {
                    echo "<input type='hidden' name='$key" . "[]' value='$value'>";                        
                }           
                echo <<<TTT

                    <div class="row">                                     
                        <div class="col-lg-3 col-md-3 col-3">
                            <span class="cell">$club_name</span>
                        </div>
                        <div class="col-lg-3 col-md-3 col-3">
                            <span class="cell">$adh_prenom</span>
                        </div>
                        <div class="col-lg-3 col-md-3 col-3">
                            <span class="cell">$adh_name</span>
                        </div>
                        <div class="col-lg-3 col-md-3 col-3">
                            <input type="number" name="score[]" id="$adh_id">
                        </div>
                    </div>                                
TTT;
                
            }/* End if array*/  
        }/* End  isset */             
    }/* End while */

    echo '</form>';
    echo '<div class="form-btn btn-participants"> <button type="submit" form="participants_scores" name="pts_scores">Caca</button></div>';

    
}/* End isset / function */

/********************************************************************************** */

if ( is_array($_POST['score'])) {
    echo <<<TET
    <div class="tableau">        
        <div class="row tab-header">        
            <div class="col-lg-3">
                <span>Club</span>           
            </div>
            <div class="col-lg-3">
                <span>Prenom</span>           
            </div>
            <div class="col-lg-3">
                <span>Nom</span>
            </div>
            <div class="col-lg-1">
                <span>Score</span>
            </div>    
        </div>
    </div>
    <div class="row">
TET;
    for ($i = 0; $i < count($_POST['score']); $i++) {

        foreach($_POST as $key=>$value) {
            if ( $key == 'club') {
                echo <<<SC_TAB
                <div class="col-lg-3 col-md-3 col-3">
                    <span class="cell">$value[$i]</span>
                </div>    
SC_TAB;
            }
            if ( $key == 'prenom') {
                echo <<<SC_TAB
                <div class="col-lg-3 col-md-3 col-3">
                    <span class="cell">$value[$i]</span>
                </div>                       
SC_TAB;
            }
            if ( $key == 'nom') {
                echo <<<SC_TAB
                <div class="col-lg-3 col-md-3 col-3">
                    <span class="cell">$value[$i]</span>
                </div>                        
SC_TAB;
            }
            if ( $key == 'score') {
                echo <<<SC_TAB
                <div class="col-lg-3 col-md-3 col-3">
                    <span class="cell">$value[$i]</span>
                </div>                                        
SC_TAB;
            }
        }
        echo '</div></div><div class="row">';
    }
}

/**************************************************************************** */

if (isset( $_POST['btn-part-adh-comp']) ) {
       /*** Data Base ***/
       $servername = "localhost";
       $username = "root";
       $password = "";

       // Create connection
       $conn = new mysqli($servername, $username, $password, 'wordpress');

       $part_name = $_POST['part_name'];
       $part_prenom = $_POST['part_prenom'];
       $part_club = $_POST['part_club'];
       $comp_name = $_POST['comp_name'];      
       
       $query_add_adh_comp = "INSERT INTO wp_competitions (`nom`,`nom_participant`, `prenom_participant`, `clubs`) VALUES ('$comp_name','$part_name','$part_prenom','$part_club')";
       $rslt_add_adh_comp = mysqli_query($conn, $query_add_adh_comp);   
     
}


/******************************* */

function list_part() {
    if ( isset( $_POST['btn-adh-comp-list'] ) ) {

            /*** Data Base ***/
            $servername = "localhost";
            $username = "root";
            $password = "";

            // Create connection
            $conn = new mysqli($servername, $username, $password, 'wordpress');

            $comp_name = $_POST['comp_name'];

            
        $query_comp = "SELECT * FROM wp_competitions WHERE `nom` = '$comp_name'";
        $rslt_comp = mysqli_query($conn, $query_comp);

                
            
            echo <<<TET
            <div class="tableau">        
                <div class="row tab-header">        
                    <div class="col-lg-2">
                        <span>Compétition</span>           
                    </div>
                    <div class="col-lg-2">
                        <span>Club</span>           
                    </div>
                    <div class="col-lg-2">
                        <span>Prenom</span>           
                    </div>
                    <div class="col-lg-2">
                        <span>Nom</span>
                    </div>
                    <div class="col-lg-1">
                        <span>Position</span>
                    </div>    
                </div>
    TET;
    
        echo ' <form action="" method="POST" id="form-pos">';    
            while ( $rows = mysqli_fetch_assoc( $rslt_comp ) ) {
                $part_name = $rows['nom_participant'];
                $part_prenom = $rows['prenom_participant'];
                $part_club = $rows['clubs'];
                $part_id = $rows['Id'];
                $comp = $rows['nom'];

                    echo <<<CP
                   
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-2">
                                <span class="cell">$comp</span>
                            </div> 
                            <div class="col-lg-2 col-md-2 col-2">
                                <span class="cell">$part_club</span>
                            </div> 
                            <div class="col-lg-2 col-md-2 col-2">
                                <span class="cell" name="">$part_prenom</span>
                                <input type="text" name="prenom[]" value="$part_prenom" style="display: none;">
                                <input type="text" name="part_id[]" value="$part_id" style="display: none;">
                            </div> 
                            <div class="col-lg-2 col-md-2 col-2">
                                <span class="cell"  name="$part_name">$part_name</span>
                                <input type="text" name="name[]" value="$part_name" style="display: none;">
                            </div> 
                            <div class="col-lg-2 col-md-2 col-2">
                               <input type="number" name="adh_pos[]">
                            </div>                            
                        </div>
                   
                    
                                
CP;
            }
            echo '<div class="row">
                    <div class="col-lg-12 col-md-12 col-12">
                        <button type="submit" form="form-pos" name="btn-pos">Enregistrer</button>
                    </div> 
                </div>
            </form>';
          
            
    }

    if ( isset( $_POST['btn-pos'])) {
            /*** Data Base ***/
            $servername = "localhost";
            $username = "root";
            $password = "";

            // Create connection
            $conn = new mysqli($servername, $username, $password, 'wordpress');

            $adh_pos = $_POST['adh_pos'];
            $part_name = $_POST['part_name'];
            $part_prenom = $_POST['part_prenom'];
            $part_id = $_POST['part_id'];

            // foreach ($adh_pos as $pos) {                    
            //     $query_scores = "UPDATE wp_competitions SET `score` = '$pos' WHERE `Id` = '$part_id'";
            //     $rslt_scores = mysqli_query($conn, $query_scores);
            // }

            for ( $i = 0; $i <= count($adh_pos); $i++) {
                $query_scores = "UPDATE wp_competitions SET `position` = '$adh_pos[$i]' WHERE `Id` = '$part_id[$i]'";
                $rslt_scores = mysqli_query($conn, $query_scores);               
            }          

       
    }
 
}






















        ?>
</section>