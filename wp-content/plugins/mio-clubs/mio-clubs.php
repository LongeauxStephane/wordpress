<?php
/*
    Plugin Name: Mio Clubs
    Description: Permet la gestion des différents clubs, de leurs adhérents ainis que des compétitions.
    Version: 1.0.0
    Author: Stéphane Longeaux
*/

require_once (plugin_dir_path(__FILE__) . '/includes/mio-clubs_inc.php');
require_once (plugin_dir_path(__FILE__) . '/includes/mio-clubs-class_inc.php');

define( 'MY_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

function include_plugin_participants_file() {
    include_once MY_PLUGIN_PATH . 'clubs.php';
}

function include_plugin_home_file() {
    include_once MY_PLUGIN_PATH . 'home.php';
}

function cmio_admin_menus_()
{
    add_menu_page(
        'cmio', //page title
        'MIO', //Menu text
        'manage_options', //Acceses level
        'cmio-home', //slug for link
        'include_plugin_home_file', //function to create principal content
//        plugin_dir_url( __FILE__) . 'admin/img/plugin-icon.jpg', //img
        'dashicons-awards',
        '10' //position in menu
    );
    //add_submenu_page( $parent_slug:string, $page_title:string, $menu_title:string, $capability:string, $menu_slug:string, $function:callable, $position:integer|null );
add_submenu_page('Clubs','Tous les clubs','Tous les clubs','Tous les clubs', 'Tous les clubs', 'include_plugin_participants_file');
add_submenu_page('Clubs','Compétitions','Compétitions','Compétitions', 'Compétitions', 'include_plugin_participants_file');
}



/* params: action*/

add_action('admin_menu', 'cmio_admin_menus_');

    

//Register Widget

function register_mio_clubs() {
    // register_widget( 'Section_Desc_Widget' );
    register_widget( 'Gestion_clubs_Widget' );
}

add_action('widgets_init', 'register_mio_clubs');

register_activation_hook( __FILE__, function () {
    touch(__DIR__ . '/mio_clubs');
});

register_deactivation_hook( __FILE__, function () {
    unlink(__DIR__ . '/mio_clubs');
});

add_action('init', function () {
    register_post_type('Clubs', [
        'label' => 'Clubs',
        'public' => true,
        
    ]);
});





