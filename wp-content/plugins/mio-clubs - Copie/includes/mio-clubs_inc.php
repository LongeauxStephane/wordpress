<?php
   function clubs_add_scripts ()
   {
       //CSS
       wp_register_style('mio-clubs-style', plugins_url( ) . '/mio-clubs/includes/less/dist/style.css');
       wp_enqueue_style('mio-clubs-style');
   
       //JS
       wp_enqueue_script('mio-clubs-script', plugins_url( ) . '/mio-clubs/includes/js/main.js');

        //Bootstrap
        wp_register_style ( 
        'bootstrap',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css' ,
        [],
        false,
        false,       
        );

        wp_enqueue_style('bootstrap');
   }
   
   add_action('admin_head', 'clubs_add_scripts');