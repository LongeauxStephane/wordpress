<?php
/**
 * Adds Section_desc widget.
 */

$pluginpath = plugin_dir_path( __FILE__ );
define('PLUGIN_PATH', $pluginpath);

class Gestion_clubs_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'section_desc_widget', // Base ID
			esc_html__( 'Description de Section', 'sd_domain' ), // Name
			array( 'description' => esc_html__( 'Permet de définir le texte de description des différentes sections du site', 'sd_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			//echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}
        //echo esc_html__( $instance['description'] , 'sd_domain' );
        global $sect_title;
        $sect_title = $instance["title"];
        $sect_desc = $instance["description"];

        echo '
            <section class="sect sect-aerien">
                <div class="container-fluid ctf-sect ctf-aerien">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col md-6 col-6">
                                <h2> ' . $instance["title"] . '</h2>
                                <div class="ct-description">
                                    ' . $instance["description"] . '
                                </div>
                            </div>
                            <div class="col-lg-6 col md-6 col-6">
                                <div class="ct-sect-img">
                                    <img src="/wp-content/uploads/images/avion.png" alt="">
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        ';
        
        // sect_naval($sect_title, $sect_desc) ;

        var_dump($instance['sect_name']);

        echo $instance['sect_name'];
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
        $sect_name = ! empty( $instance['sect_name'] ) ? $instance['sect_name'] : esc_html__( 'Nom de section', 'sd_domain' );
        $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'Section description', 'sd_domain' );
        $description = ! empty( $instance['description'] ) ? $instance['description'] : esc_html__( 'Description', 'sd_domain' );
        ?>


        <div class="row">
            <div class="col-lg-12">
                <label for="<?php echo esc_attr( $this->get_field_id( 'section' ) ); ?>">
                    <?php esc_attr_e( 'Section:', 'sd_domain' ); ?>
                </label>                    
                    <select id="<?php echo $this->get_field_name( 'sect_name' ); ?>" name="<?php echo $this->get_field_name( 'sect_name' ); ?>">
                        <option value="sct-home">Accueil</option>
                        <option value="sct-aerien">Aérien</option>
                        <option value="sct-naval">Naval</option>
                        <option value="sct-automobile">Automobile</option>
                    </select>
            </div>            
        </div>

        <div class="col-lg-12">
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>">
                <?php esc_attr_e( 'Titre:', 'sd_domain' ); ?>
            </label> 
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </div>

        <div class="col-lg-12">
            <!-- <textarea name="sect-description" id="sect-description" ></textarea> -->
            <textarea id="<?php echo esc_attr( $this->get_field_id( 'description' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'description' ) ); ?>" type="text" value="<?php echo esc_attr( $description ); ?>"></textarea>
        </div>

		<?php 
    }
    
    /************************************ */
    function sect_naval( $sect_title, $sect_desc) 
    {
        echo '
            <section class="sect sect-aerien">
                <div class="container-fluid ctf-sect ctf-aerien">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col md-6 col-6">
                                <h2> ' . $sect_title . $instance["title"] . '</h2>
                                <div class="ct-description">
                                    ' . $sect_desc . '
                                </div>
                            </div>
                            <div class="col-lg-6 col md-6 col-6">
                                <div class="ct-sect-img">
                                    <img src="/wp-content/uploads/images/avion.png" alt="">
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        ';
        var_dump($instance['title']);
    }

    /************************************ */

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['sect_name'] = ( ! empty( $new_instance['sect_name'] ) ) ? sanitize_text_field( $new_instance['sect_name'] ) : '';
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
		$instance['description'] = ( ! empty( $new_instance['description'] ) ) ? sanitize_text_field( $new_instance['description'] ) : '';

		return $instance;
	}

} // class Foo_Widget

?>