<?php
    require_once (plugin_dir_path(__FILE__) . '/includes/mio-clubs_inc.php');
    require_once (plugin_dir_path(__FILE__) . '/includes/mio-clubs-class_inc.php');
?>



<section class="admin-gestion">

    <h1>Mio</h1>

    <div class="row">
        <h3>Liste des clubs</h3>
    </div>



    <div class="tableau">        
        <div class="row tab-header">        
            <div class="col-lg-1">
                <span>Selection</span>           
            </div>
            <div class="col-lg-3">
                <span>Nom</span>           
            </div>
            <div class="col-lg-3">
                <span>Adresse</span>
            </div>
            <div class="col-lg-1">
                <span>Code Postal</span>
            </div>
            <div class="col-lg-1">
                <span>Téléphone</span>
            </div>
            <div class="col-lg-2">
                <span>Domaine</span>
            </div>
        </div>
        

        <div class="clubs-list">
            <?php clubs_list(); ?>
        </div>



        <div class="clubs-form">
            <?php clubs_add (); ?>
        </div>
       
       
    </div>
       




   










<?php

    /*** Data Base ***/
    $servername = "localhost";
    $username = "root";
    $password = "";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, 'wordpress');


function clubs_list(string $list = '')

{

    /*** Data Base ***/
$servername = "localhost";
$username = "root";
$password = "";

// Create connection
$conn = new mysqli($servername, $username, $password, 'wordpress');

 
$query_clubs = "SELECT * FROM wp_clubs";
$rslt_clubs = mysqli_query($conn, $query_clubs);

    while ( $rows = mysqli_fetch_assoc($rslt_clubs) ) {
        $club_id = $rows['id'];
        $club_name = utf8_encode($rows['nom']);
        $club_adress = utf8_encode($rows['adresse']);
        $club_cp = $rows['cp'];
        $club_mail = $rows['email'];
        $club_tel = $rows['tel'];
        $club_domaine = $rows['domaine'];
            

echo <<< CLUBS_LIST
 
<div class="row">
    <div class="col-lg-1 col-md-1 col-1">
        <input type="checkbox" id="$club_id" name="user_pick[]" >
    </div>
    <div class="col-lg-3 col-md-3 col-3">
        <span class="cell">$club_name</span>
    </div>
    <div class="col-lg-3 col-md-3 col-3">
        <span class="cell">$club_adress</span>
    </div>
    <div class="col-lg-1 col-md-1 col-1">
        <span class="cell">$club_cp</span>
    </div>
    <div class="col-lg-1 col-md-1 col-1">
        <span class="cell">$club_tel</span>
    </div>
    <div class="col-lg-2 col-md-2 col-2">
        <span class="cell">$club_domaine</span>
    </div>
</div>      

               
CLUBS_LIST;  

// if ($list = 'list_clubs') {
//     echo <<< USERS_LIST
//         <div class="row">
//             <div class="col-lg-3 col-md-3 col-3">
//                 <span class="cell">$club_name</span>
//             </div>
//         </div>     
               
//     USERS_LIST;  

// }

    }/*Fin while*/
}
?>

<!---------------------------------------------------------->

<!--========== Ajout d'utilisateurs =========*/-->


<?php
    /* Ajout d'utilisateurs*/
    if ( isset($_POST['btn_clubs_add']) ) {      
        $club_name_added = $_POST['club_name_add'];
        $club_adresse_added = $_POST['club_adresse_add'];
        $club_cp_added = $_POST['club_cp_add'];
        $club_tel_added = $_POST['club_tel_add'];
        $club_domaine_added = $_POST['club_domaine_add'];


        $add_club_name = "INSERT INTO wp_clubs(nom, adresse, cp, tel, domaine) VALUES('$club_name_added', '$club_adresse_added', '$club_cp_added', '$club_tel_added', '$club_domaine_added')";
        $add_club_rslt = mysqli_query($conn, $add_club_name);      
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);        
    }   

    function clubs_add ()
    {
        if ( !isset($_GET["btn-ajouter"]) ) {            
            echo <<< clubS_ADD
                <div class="row">
                    <form action="" method="POST" class="formulaire-ajout">
                        <div class="col-lg-12 col-md-12 col-12">
                            <label for="club_name_add">Nom du club</label>
                                <input type="text" name="club_name_add">        
                            <label for="club_adresse_add">Adresse</label>
                                <input type="text" name="club_adresse_add">        
                            <label for="club_cp_add">Code postale</label>
                                <input type="text" name="club_cp_add">    
                            <label for="club_tel_add">Téléphone</label>
                                <input type="text" name="club_tel_add">                       
                            <select name="club_domaine_add" id="">
                                <option value="automobile" name="automobile">Automobile</option>
                                <option value="aerien" name="aerien">Aérien</option>              
                                <option value="naval" name="naval">Naval</option>              
                            </select>    
                            <button type="submit" name="btn_clubs_add" >Valider</button>      
                        </div>            
                    </form>
                </div>
                
            clubS_ADD; 
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);       
        }

    }

    ?>


</section>