<?php 

require_once 'functions.php'; 

class MioScoresWidget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct('scores_widget', 'Mio Scores Widget' );
    }

    public function widget ($arg, $instance)
    {
        echo $args['before_widget'];

        if ( isset( $instance['title'] ) ) {
            $title = apply_filters( 'widget_title', $instance['title'] );
            echo '<h4>' . $args['before_title'] . $title . $args['after_title'] . '</h4>';
        }

       list_rslt ();

        echo $args['after_widget'];

        
    }

    public function form ($instance)
    {
        $title = isset($instance['title']) ? $instance['title'] : '';
        ?>
        <p>
            <label for="<?= $this->get_field_id('title')?>">Titre</label>
            <input
                type="text"
                name="<?= $this->get_field_name('title')?>"
                value="<?php esc_attr($title) ?>"
                class="widefat"
                id="<?= $this->get_field_id('title')?>"> 
        </p>
        <?php
    }

    public function update ($newInstance, $oldInstance)
    {
        return  $newInstance;
        
    }


}