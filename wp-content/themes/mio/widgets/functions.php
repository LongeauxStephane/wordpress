<?php

function list_rslt () {
    /*** Data Base ***/
    $servername = "localhost";
    $username = "root";
    $password = "";

    // Create connection
    $conn = new mysqli($servername, $username, $password, 'wordpress');

    $query_rslt = "SELECT * FROM  wp_competitions ORDER BY `nom`, `position` ASC";
    $rslt_rslt = mysqli_query($conn, $query_rslt);   

    echo<<<TB_RSLT_HD
    <div class="row">
          <div class="col-lg-4 col-md-4 col-4">
              <span class="cell">Competition</span>
          </div>              
          <div class="col-lg-2 col-md-2 col-2">
              <span class="cell"  name="">Pos</span>                
          </div>             
          <div class="col-lg-3 col-md-3 col-3">
              <span class="cell" name="$part_prenom">Prénom</span>
          </div>               
          <div class="col-lg-3 col-md-3 col-3">
              <span class="cell"  name="$part_name">Nom</span>                
          </div> 
      </div>
TB_RSLT_HD;

      while ( $rows = mysqli_fetch_assoc(  $rslt_rslt ) ) {
          $part_name = $rows['nom_participant'];
          $part_prenom = $rows['prenom_participant'];
          $part_pos = $rows['position'];
          $part_club = $rows['clubs'];
          $comp = $rows['nom'];

          echo <<<TB_RSLT
          <div class="row">
              <div class="col-lg-4 col-md-4 col-4">
                  <span class="cell">$comp</span>
              </div>              
              <div class="col-lg-2 col-md-2 col-2">
                  <span class="cell"  name="">$part_pos</span>                
              </div>             
              <div class="col-lg-3 col-md-3 col-3">
                  <span class="cell" name="">$part_prenom</span>
              </div>               
              <div class="col-lg-3 col-md-3 col-3">
                  <span class="cell"  name="$part_name">$part_name</span>                
              </div> 
          </div>
TB_RSLT;

      
      }
}