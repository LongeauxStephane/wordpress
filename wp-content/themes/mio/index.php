
<!-- includes -->
<?php get_header(); ?>
<?php get_sidebar(); ?>

<!-- Pages link -->
<?php 
    $link = '/?page_id=' . get_the_ID();
?>


<main>
    <div class="container-fluid ctf-sect ctf-main-desc">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="box-main">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-6">
                                <div class="box--main-desc">
                                    <div class="box-title">
                                        <h1>      
                                            <a href="<?php echo get_bloginfo('wpurl'); ?>"> <?php echo get_bloginfo('name'); ?>   </a>
                                        </h1>
                                        <span class="sub-title">Modélisme interdépartementale Occitant</span>
                                    </div>                                    
                                    <div class="box--main-ct-description ct-description">
                                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquam eius impedit ratione architecto dolorem necessitatibus quae, nemo adipisci incidunt ipsa sed ex explicabo aliquid velit repellendus commodi magnam, ea molestiae!</p>
                                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquam eius impedit ratione architecto dolorem necessitatibus quae, nemo adipisci incidunt ipsa sed ex explicabo aliquid velit repellendus commodi magnam, ea molestiae!</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-6">
                                <div class="box--main-img">
                                    <img src="/wp-content/uploads/images/voiture.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</main>


<?php the_content(); ?>
<!--  -->

<section class="sect sect-aerien">
    <div class="container-fluid ctf-sect ctf-aerien">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col md-6 col-6">
                    <a href="/?page_id=58" target="_blank">
                        <h2>Aérien</h2>
                    </a>
                    <div class="ct-description">
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquam eius impedit ratione architecto dolorem necessitatibus quae, nemo adipisci incidunt ipsa sed ex explicabo aliquid velit repellendus commodi magnam, ea molestiae!</p>
                    </div>
                </div>
                <div class="col-lg-6 col md-6 col-6">
                    <div class="ct-sect-img">
                        <a href="/?page_id=58" target="_blank"><img src="/wp-content/uploads/images/avion.png" alt=""></a>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</section>

<section class="sect sect-naval">
    <div class="container-fluid ctf-naval">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col md-6 col-6">
                    <a href="/?page_id=51" target="_blank">
                        <h2>Naval</h2>
                    </a>
                    <div class="ct-description">
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquam eius impedit ratione architecto dolorem necessitatibus quae, nemo adipisci incidunt ipsa sed ex explicabo aliquid velit repellendus commodi magnam, ea molestiae!</p>
                    </div>
                </div>
                <div class="col-lg-6 col md-6 col-6">
                    <div class="ct-sect-img">
                        <a href="/?page_id=51" target="_blank"><img src="/wp-content/uploads/images/bateau.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>   
</section>


<section class="sect sect-automobile">
    <div class="container-fluid ctf-sect ctf-automobile">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col md-6 col-6">
                    <a href="/?page_id=46" target="_blank">
                        <h2>Automobile</h2>
                    </a>
                    <div class="ct-description">
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquam eius impedit ratione architecto dolorem necessitatibus quae, nemo adipisci incidunt ipsa sed ex explicabo aliquid velit repellendus commodi magnam, ea molestiae!</p>
                    </div>
                </div>
                <div class="col-lg-6 col md-6 col-6">
                    <div class="ct-sect-img">
                        <a href="/?page_id=46" target="_blank">
                            <img src="/wp-content/uploads/images/automobile.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    
<?php get_footer(); ?>