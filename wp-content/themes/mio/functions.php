<?php
   require_once 'widgets/MioScoresWidget.php';

   function mio_widget_init()
    {
       register_widget( MioScoresWidget::class );
        register_sidebar(
            array (
                'name' => 'sidebar',
                'description' => __('les widgets de la sidebar', 'ern2020'),
                'id' => 'sidebar',
                'before_widget' => '<div class="border border-info mb-3 rounded p-2">',
                'after_widget' => '</div>',
                'before_title' => '<h4 class="widget-title">',
                'after_title' => '</h4>'
            )
        );
        register_widget('Mio_Desc_Plugin');        
    }

   
    function mio_register_assets()
    {
       //Bootstrap
        wp_register_style ( 
           'bootstrap',
           'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css' ,
           [],
           false,
           false,       
        );

        wp_enqueue_style('bootstrap');

        wp_register_script(
           'bootstrat-cdn',
           'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js',
            [],
            false,
            true,
        );

        wp_enqueue_style('bootstrap-cdn');

        wp_register_script(
           'bootstrat-jq',
           'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js',
            [],
            false,
            true,
        );

        wp_enqueue_style('bootstrap-jq');

        //Scripts
         wp_enqueue_script('scripts', '/wp-content/themes/mio/js/main.js', true);   
        


        //Style css
        wp_enqueue_style('style', '/wp-content/themes/mio/less/dist/style.css', false);   

        //fonts
        wp_register_script(
            'fonts',
            '<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,700;1,300;1,400;1,500&display=swap" rel="stylesheet">',
            false,
            false,
        );

        wp_enqueue_style('fonts');

    }

    function mio_supports()
    {
        add_theme_support( 'Menus');
        register_nav_menu( 'header', 'Position 1: En haut du site');
        register_nav_menu( 'footer', 'Position 20: Pied de page');
    }

    function mio_menu_class (array $classes): array
    {
        $classes[] = 'nav-item';
        return $classes;
    }    

    /*********************************** */
    class Mio_Desc_Plugin extends Wp_Widget
    {
    // initialisation du widget dans le backend
    public function __construct()
    {
       $widget_ops = array(
          'className' => 'ern_meteo',  // on définit un alias pour le className
          'description' => __('Information de météo pour le wp de l\'Ern'),   // ajout d'une description
          'customize_selective_refresh' => true,   // rafraichissement automatique du widget sur le frontend
       );
       // appel du constructeur de la classe parente
       // (id 'weather' qui servira en bdd, alias 'Meteo Widget', 'Meteo' qui serviront à l'affichage dans le backend
       parent::__construct('weather', __('Meteo Widget', 'Meteo'), $widget_ops);
    }
  

    // construction du formulaire de backend
    public function form($instance)
    {
       // (array) est le "cast" de l' $instance
       $instance = wp_parse_args( (array) $instance, array (
            // ici on définit les variables dont on va avoir besoin dans le cadre du widget
            'lat' => '',
            'lon' => '',
            'apitoken' => ''
       )
      );
      ?>
         <!-- dans wp on peut se permettre de couper le flux php pour insérer du html - si si c'est vrai !  -->
         <p>
            <!-- this récupère l'id du champ 'lat' de l'instance -->
            <label for="<?php echo $this->get_field_id('lat'); ?>">Latitude</label>
            <input class="widefat" 
               id="<?php echo $this->get_field_id('lat'); ?>" 
               type="text" 
               name="<?php echo $this->get_field_name('lat'); ?>" 
               value="<?php echo esc_attr($instance['lat']); ?>" />  <!-- on va chercher la value dans le tableau $instance -->
         </p>
         <p>
            <label for="<?php echo $this->get_field_id('lon'); ?>">Longitude</label>
            <input class="widefat" 
                  id="<?php echo $this->get_field_id('lon'); ?>" 
                  type="text" 
                  name="<?php echo $this->get_field_name('lon'); ?>" 
                  value="<?php echo esc_attr($instance['lon']); ?>" />
         </p>
         <p>
            <label for="<?php echo $this->get_field_id ('apitoken');?>">Code API</label>
            <input class="widefat"
                  type="text" 
                  id="<?php echo $this->get_field_id ('apitoken');?>" 
                  name="<?php echo $this->get_field_name ('apitoken');?>" 
                  value="<?php echo esc_attr($instance['apitoken']);?>"/>
         </p>
         
      <?php
    }

   // mise à jour des données
   public function update($new_instance, $old_instance) 
   {
      $instance = $old_instance;
      $instance['lat'] = sanitize_text_field( $new_instance['lat'] );
      $instance['lon'] = sanitize_text_field( $new_instance['lon'] );
      $instance['apitoken'] = sanitize_text_field( $new_instance['apitoken'] );

      return $instance;
   }

   /**
    * affichage frontend
   * @param array $args
   * @param array $instance
   */

   public function widget ($args, $instance) 
   {
         $title = 'Météo';
         // construction de l'appel api
         $url = 'https://api.darksky.net/forecast/'.$instance['apitoken'].'/'.$instance['lat'].','.$instance['lon'];

         $request = wp_remote_get($url);
         if ( is_wp_error($request) ) 
         {
            return false;
         }
         $body = wp_remote_retrieve_body ( $request );
         $data = json_decode( $body );

         // on récupère l'argument de widget défini dans "register_sidebar"
         echo $args['before_widget'];
         if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
         }

         echo '<div class="meteo_wrap" id="meteo_wrap">';
            if ( !empty($data) ) {
               // s'il pleut, la class sera "rain" et l'icone sera modifiée en conséquence
               echo '<div class="'.$data->currently->icon.' icon">&nbsp;</div>';
               // on convertit les Farenheit en Celsius
               echo '<div>'.number_format((($data->currently->temperature - 32) * (5/9)), 2, ',', ' ').' deg</div>';
               // on convertit les miles per hour en km/heure
               echo '<div>'.number_format(($data->currently->windSpeed * 1.609), 2, ',', ' ').' km/h</div>';
            }
         echo '</div>';
         echo $args['after_widget'];
   }
}

    /******************************** */

    add_action( 'widgets_init', 'mio_widget_init' );

    add_action( 'after_setup_theme', 'mio_supports' );
    add_action( 'wp_enqueue_scripts', 'mio_register_assets' );
   
    add_theme_support('title-tag');
    add_theme_support( 'woocommerce' ); 

    add_filter('nav_menu_css_class', 'mio_menu_class');

    
?>