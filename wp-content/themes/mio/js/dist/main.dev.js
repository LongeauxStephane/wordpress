"use strict";

document.addEventListener("DOMContentLoaded", function (event) {
  var body = document.querySelector('body');
  var woo = document.querySelector('.woocommerce');

  if (woo.parentNode != body) {
    body.classList.add("woo-page");
  }
});