<?php

   get_header();

?>

<div class="page">

    <div class="container">
        <div class="row">
            <div class="col-sm-12 blog-main">  
                <div class="art-content">
                    <h1 class="art-title">
                        <?php                                  
                            if ( have_posts() ) : while ( have_posts() ) : the_post();  
                                the_title(); 
                            endwhile; endif;                     
                        ?>
                    </h1>                
                    <div class="art-desc">
                        <?php         
                            if ( have_posts() ) : while ( have_posts() ) : the_post();  
                                
                                echo '<div>' . the_content() . '</div>'; // displays whatever you wrote in the wordpress editor
                            endwhile; endif; //ends the loop        
                        ?>
                    </div>  
                </div> 
            </div>
        </div>  
    </div>

</div>
 


<?php

   get_footer();

?>