<div class="container-fluid ctf-footer">
    <div class="container">
        <footer>
            <?php
                wp_nav_menu ([
                    'theme_location' => 'footer',
                    'container' => false,
                    'menu_class' => 'navbar-nav'
                ]);
            ?>
        </footer>
    </div>
</div>


<?php wp_footer( ); ?>
</body>
</html>