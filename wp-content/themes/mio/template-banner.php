<?php
/*
    Template Name: MIO Template
*/
?>

<?php
    // s'il y a des posts : tant qu'il y a des posts :
    if( have_posts() ) : while ( have_posts() ) : the_post();
        get_template_part( 'content', get_post_format() );
    endwhile; endif;
?>