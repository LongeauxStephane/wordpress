<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         's[$mTH$h[qL=F-KV[D#Z5Wz=4%7zlejTf+1YSh=-C(>)kF0],]~/N]k2Dm;K3O*E' );
define( 'SECURE_AUTH_KEY',  '<c(vBJ]a:^Iyb.AIme,a`LLIoDG6!kf%+nJZGPHj.f<.N>F6#[NcZmxmN1WihnqD' );
define( 'LOGGED_IN_KEY',    'qqXbSi7OzebAlM2#C32OzQ7l*V*^^?uEKY5F3HU*rNrDo$#eb1=L{}Y<V8:DUa4f' );
define( 'NONCE_KEY',        'RPHe&=SC$alr%>MQ]3;p*@pM}pM-F9d]bn+9V3r|8a+8<NKmOF#|s)^|?_rJ91_#' );
define( 'AUTH_SALT',        'YA+Booi(En6cRB3gDx}b2*W(CkJG}9$&-yezl{:C.;jxl}7NL4CQ|c2$BF0~N!Ig' );
define( 'SECURE_AUTH_SALT', ':ZmbW#i{] 0Y&^:Ky`/fh*:G^Q&3?>hh{%?c?X`H2yI$A:gvl[t%!KtZ H7wFVV7' );
define( 'LOGGED_IN_SALT',   'OR8/0c:J0n*DW4!nGau/89Ga](bv[m-gI~vF0t/F@  Eulp@qJ93HDEGBt7#Vv~E' );
define( 'NONCE_SALT',       '!7x+K2wa3Xr0UjShegmjo1F!FQNn7QyXXEdN3S1P*[J`j%>bvU%n&c|f(505Y1if' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
